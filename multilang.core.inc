<?php
/**
 * @file
 * Multilang module core functions.
 */

/* =============================================================== CORE FUNCS */
global $language, $base_path;
define('_MULTILANG', $language->language);
define('_MULTILANG_NAME', $language->native);
define('_MULTILANG_LEN', strlen(_MULTILANG));
define('_MULTILANG_PATH', $base_path);
define('_MULTILANG_SP', '(?:&nbsp;|\s|\xc2\xa0)*');
define('_MULTILANG_BR', '(?:<\/p>' . _MULTILANG_SP . '<p>|<br ?\/?>)*');
define('_MULTILANG_DIRTY',
  '(?:'. _MULTILANG_SP . _MULTILANG_BR . _MULTILANG_SP . ')*');
module_load_include('inc', 'multilang', 'multilang.data');
/* ---------------------------------------------------------------- MAIN FUNC */
/**
 * Looks for "multi" segments and reduces each one to the current language part.
 *
 * @param string $text
 *   The piece of text to be processed.
 *
 * @param bool $links
 *   (optional, default FALSE)
 *   Notifies if internal links must be processed too.
 *
 * @param bool $default
 *   (optional, default FALSE)
 *   This param has a special meaning:
 *   - when FALSE, the current language is searched for; then if not found,
 *     fallbacks looking for the site-native language.
 *   - when TRUE, the site-native language is searched for, whatever the current
 *     language is
 *   - when NULL, the current language is searched for, and fallback never
 *     happens (returns empty text if current language not found)
 *
 * @return string
 *   The resulting content of given $text after processing.
 */
function _multilang_process($text, $links = FALSE, $default = FALSE) {
  /*
  Prepare
  ------- */
  global $_multilang_language, $_multilang_default;
  // Define required (default or current) language.
  $_multilang_language = $default ? language_default()->language : _MULTILANG;
  // Register $default option for the current nesting level.
  $_multilang_default[] = $default;
  // Normalize newlines (\n only).
  $text = str_replace(array("\r\n", "\r"), "\n", $text);
  /*
  Process [multi] tags
  --------------------
  Normalize "multi" tags into [multi]:
  . <multi> and </multi> (direct SPIP migration, or Multilang plugin)
  . &lt;multi&gt; and &lt;/multi&gt; (input SPIP syntax through CKEditor)
  . suppress any wrapper like "<p>[multi]</p>" */
  $text = preg_replace(
    '`(?:<p>)?\[(/?)multi\](?:</p>)?`s',
    '[$1multi]',
    $text);
  // Return the text blocks marked with the required language.
  $text = preg_replace_callback(
    '`\[multi\](.*?)\[/multi\]`is',
    '_multilang_process_multi',
    // Don't use anonymous function, available only with PHP >= 5.3.
    $text);
  /*
  Process links, if required
  --------------------------
  Ensure to have the current language as URL prefix. */
  if ($links) {
    $text = preg_replace_callback(
      // Look for href value.
      '`href\s*=\s*"([^"]*)"`is',
      '_multilang_process_link',
      // Don't use anonymous function, available only with PHP >= 5.3.
      $text);
  }
  /*
  Finalize
  -------- */
  // Unregister $default option for the current nesting level.
  unset($_multilang_default[count($_multilang_default) - 1]);
  // Render selected text.
  return $text;
}
/* ----------------------------------------------------------- CALLBACK FUNCS */
/**
 * Extract and return block marked with the required language.
 */
function _multilang_process_multi($matches) {
  global $_multilang_language, $_multilang_default;
  $fallback_allowed = (end($_multilang_default) !== NULL);
  $cur_lang = $_multilang_language;
  while (TRUE) {
    // Get other languages list (used as limit for regex).
    $other_langs = array();
    foreach (language_list() as $lang) {
      if ($lang->language <> $cur_lang) {
        $other_langs[] = '\[' . $lang->language . '\]';
      }
    }
    // Look for a translation available for the required language.
    preg_match(
      $regex =
      // Look for required lang mark.
      '`\[' . $cur_lang . '\]' .
      // Look for content, stripped from any kind of pollution.
      _MULTILANG_DIRTY . '(.*?)' . _MULTILANG_DIRTY .
      // Look for other lang mark, or end.
      '(?:' . implode('|', $other_langs) . '|$)`is',
      // $matches[1] contains text found between [multi] and [/multi] tags.
      $matches[1],
      $found
    );
    // Try to fallback if required language not found.
    if (
      // No translation found.
      preg_match(
        '`^(\.*' . _MULTILANG_NAME . '\.*| |\s|<p>|</p>|&nbsp;|\xc2\xa0)*$`i',
        @$found[1]
      /* Above regex looks for, either:
      - simply language name like "...Language..." (from CKEditor default)
      - or empty or blank part
      - or language not found
      (c2a0 is the UTF8 NO-BREAK-SPACE code sometimes returned by CKEditor) */
      )
      and
      // Not already tried fallback with no success.
      ($cur_lang <> 'en' or $fallback_allowed)
      and
      // Currently not using native site language yet when fallback is allowed.
      ($cur_lang <> language_default()->language and $fallback_allowed)
    ) {
      // Retry with native site language, or with [en] if fallback forbidden.
      $cur_lang = $fallback_allowed ? language_default()->language : 'en';
      continue;
    }
    break;
  }
  return trim(@$found[1]);
  // "@" above: because the current language-mark may be absent.
  // trim() above: avoids superfluous \n in the returned text.
}
/**
 * Enhance internal links.
 *
 * Use alias, control lang segment.
 */
function _multilang_process_link($matches) {
  $href = @$matches[1];
  if (!preg_match(
    '`^((https?|s?ftp|mailto):|' . _MULTILANG_PATH . '|#)`i', $href)
  ) {
    // Do nothing if complete or absolute url, or mailto, or simple anchor.
    // Turn bare link into internal, adding langcode if not already present.
    $href = _MULTILANG_PATH .
      (substr($href, 0, _MULTILANG_LEN + 1) == _MULTILANG . '/' ?
        NULL
      :
        (_MULTILANG . '/')
      ) .
      $href;
    // Replace "node/id" by its alias, if needed.
    $href = preg_replace_callback('`node/\d+`i', '_multilang_set_alias', $href);
  }
  return 'href="' . $href . '"';
}
/**
 * Return alias from given "node/%" path.
 */
function _multilang_set_alias($matches) {
  return drupal_get_path_alias($matches[0]);
}
/* ========================================================================== */
