<?php
/**
 * @file
 * Multilang module text data.
 *
 * Contains all texts translations, written using "multi" syntax.
 */

/* ================================================================== INSTALL */
/**
 * Informative message after module activation.
 */
function _multilang_install() {
  module_load_include('inc', 'multilang', 'multilang.core');
  ob_start();
?>
[multi]
[en]The <em>Multilang</em> module was successfully installed.<br />
Now you may use the "multi" syntax inside of text fields, in any type of 
conten, in blocks and in taxonomy terms.<br />
NOTE: in order to use the
<a href="<?php echo _MULBASE_PATH; ?>/en/admin/help/multilang#ckeditor">
advanced features</a>
available with CKEditor, you must install the <em>Widget</em> plugin.
[fr]Le module <em>Multilang</em> a été installé avec succès.<br />
Vous pouvez désormais utiliser la syntaxe "multi" à l\'intérieur des champs
texte, dans n\'importe quel type de contenu ainsi que dans les blocs et les
termes de taxonomie.<br />
NOTA : pour utiliser les
<a href="<?php echo _MULBASE_PATH; ?>/fr/admin/help/multilang#ckeditor">
possibilités avancées</a> disponibles avec CKEditor, vous devez installer le
plugin <em>Widget</em>.
[/multi]
<?php
  return _multilang_process(ob_get_clean());
}
/* ================================================================= FALLBACK */
/**
 * Tip displayed when a "multi" segment doesn't contain the required language.
 */
function _multilang_fallback() {
  module_load_include('inc', 'multilang', 'multilang.core');
  ob_start();
?>
[multi]
[en]This section is not translated yet. We apologize for any inconvenience.
[fr]Cette section n'a pas encore été traduite. Merci de votre compréhension.
[/multi]
<?php
  // Special case: default=NULL notifies not to fallback when required
  // language is not found (or would enter infinite loop).
  return _multilang_process(ob_get_clean(), /*links*/FALSE, /*default*/NULL);
}
/* =================================================================== TOKENS */
/**
 * Localized tokens characteristics.
 */
function _multilang_token_adds() {
  return array(
    'signature' => '
[multi]
[en]with Multilang
[fr]avec Multilang
[/multi]
    ',
    'native-short' => '
[multi]
[en](site language)
[fr](langue du site)
[/multi]
    ',
    'native-long' => '
[multi]
[en]in the default site language
[fr]dans la langue du site par défaut
[/multi]
    ',
  );
}
/*
function _multilang_token_info() {
  return array(
    'node' => array(
      // [node:title]
      'title' => array(
        'name' => '
[multi]
[en]Title
[fr]Titre
[/multi]
        ',
        'description' => '
[multi]
[en]The node title
[fr]Le titre du noeud
[/multi]
        ',
      ),
    ),
    'term' => array(
      // [term:name]
      'name' => array(
        'name' => '
[multi]
[en]Name
[fr]Nom
[/multi]
        ',
        'description' => '
[multi]
[en]The taxonomy term name
[fr]Le nom du terme de taxonomie
[/multi]
        ',
      ),
      // [term:description]
      'description' => array(
        'name' => '
[multi]
[en]Description
[fr]Description
[/multi]
        ',
        'description' => '
[multi]
[en]The optional description of the taxonomy term
[fr]La description facultative du terme de taxonomie
[/multi]
        ',
      ),
    ),
    'vocabulary' => array(
      // [vocabulary:name]
      'name' => array(
        'name' => '
[multi]
[en]Name
[fr]Nom
[/multi]
        ',
        'description' => '
[multi]
[en]The taxonomy vocabulary name
[fr]Le nom du vocabulaire de taxonomie
[/multi]
        ',
      ),
      // [vocabulary:description]
      'description' => array(
        'name' => '
[multi]
[en]Description
[fr]Description
[/multi]
        ',
        'description' => '
[multi]
[en]The optional description of the taxonomy vocabulary
[fr]La description facultative du vocabulaire de taxonomie
[/multi]
        ',
      ),
    ),
  );
}
*/
/* ================================================================ JS WIDGET */
/**
 * Informative text used in multilang.js widget.
 */
function _multilang_js_widget() {
  module_load_include('inc', 'multilang', 'multilang.core');
  ob_start();
  // CAUTION: this is JSON data.
?>
{
  "backText":
[multi]
[en]"Back"
[fr]"Annuler"
[/multi],
  "inputTitle":
[multi]
[en]"Click to display a per-language input area"
[fr]"Cliquer pour afficher une zone de saisie par langue"
[/multi],
  "backTitle":
[multi]
[en]"Restore previous content"
[fr]"Restaurer le contenu précédent"
[/multi],
  "noticeEmpty":
[multi]
[en]"empty @langNative text"
[fr]"texte @langNative vide"
[/multi],
  "useText":
[multi]
[en]"Use Multilang template"
[fr]"Utiliser le gabarit Multilang"
[/multi],
  "useTitle":
[multi]
[en]"Reformat this area depending on currently defined languages"
[fr]"Reformater cette zone en fonction des langues actuellement définies"
[/multi]
}
<?php
  return _multilang_process(ob_get_clean());
}
/* ================================================================= CKEDITOR */
/**
 * Informative text displayed in CKEditor config plugins list.
 */
function _multilang_ckeditor_plugin() {
  module_load_include('inc', 'multilang', 'multilang.core');
  ob_start();
?>
[multi]
[en]Multilang - Plugin to embed multiple languages translations in the same 
field
[fr]Multilang - Un plugin pour intégrer des traductions en plusieurs langues 
dans le même champ
[/multi]
<?php
  return _multilang_process(ob_get_clean());
}
/* ===================================================================== HELP */
/**
 * Complete documentation.
 */
function _multilang_help() {
  module_load_include('inc', 'multilang', 'multilang.core');
  ob_start();
?>
<h3>
  <em>Multilang</em>
</h3>
[multi]
[en]
<p>
  This module is intended to allow entering all desired translations of a given
  content inside of a unique node, instead of having to create a separate node
  (with <em>Multilingual Content</em>) or a distinct field version (with
  <em>Entity Translation</em>) for each translation.
</p>
<p>
  The benefits of using this method are mainly:
  <ul>
    <li>when entering new content or updating an existing one, authors keep a
    total visibility upon all the translations at the same time</li>
    <li>a direct consequence is to reduce the risk of omitting to update the
    translations when an original content has been modified</li>
    <li>when a content includes a lot of lang-insensitive data (like images,
    links, or simply verbous HTML with a number of attributes), these parts
    don't have to be duplicated: only the textual parts must be entered as
    "multi" and rewritten in the different languages</li>
    <li>since it emulates the SPIP <code>&lt;&nbsp;multi&nbsp;&gt;</code>
    syntax, this method is nicely suitable to allow the direct migration of
    contents from this CMS, without the need of a painful data restructuration
  </li>
    <li>moreover, in the latter case, authors can maintain their previous work
    habits unchanged</li>
  </ul>
</p>
<section>
<h3 class="multilang-toggler" id="install">
  Installation/Configuration
</h3>
<p>
  <em>Multilang</em> works without the need of other translation module,
  assumed you have set the "Internationalization" option of <em>i18n</em>, and
  configured the desired languages. Nevertheless you can simultaneously use
  alternative translation methods, like <em>Multilingual Content</em>,
  <em>Menu translation</em> etc.
</p>
<p>
  By itself, the module contains no configuration option. By cons, to use the
  "multi" syntax you must:
  <ul>
    <li>
      In "Administer > Configuration > Regional and language > Languages",
      enable the "URL" detection method, and configure it to "Path prefix"</li>
    <li>
      For each enabled language, ensure to provide the corresponding standard
      "Path prefix language code" (don't leave it empty)</li>
    <li>
      Position the <em>Language switcher (User interface text)</em> block in a
      region, anywhere you like</li>
  </ul>
</p>
<p>
  If you're also using other options of the <em>i18n</em> module, there are
  some additional configuration options you must pay attention to:
  <ul>
    <li>
      for each node where the "multi" syntax is used, you MUST select the
      <strong>Neutral language</strong> in the "Language" field (or the lang
      switcher would deactivate any lang other than the specified one)</li>
    <li>
      for each block where the the "multi" syntax is used, you MUST
      <strong>keep unchecked all the proposed languages</strong> in the
      "Languages" part of "Visibility settings"</li>
  </ul>
</p>
</section><section>
<h3 class="multilang-toggler" id="syntax">
  Syntax
</h3>
<p>
  NOTE: this is the raw internal structure which makes the <em>Multilang</em>
  mechanism to work, and was initially designed to easily migrate data from
  the <em>SPIP</em> CMS.<br />
  As of 7.x-2.0 you can get totally rid of directly managing the "multi"
  syntax (look at <strong>User interface</strong> below). However you can
  continue to use whenever you want.
</p>
<p>
  When entering data in a text area, you may insert multilingual pieces of
  text by enclosing them between <code>&#91;&nbsp;multi&nbsp;&#93;</code> and
  <code>&#91;&nbsp;/multi&nbsp;&#93;</code> tags.<br />
  Such a piece of text is a "multi" <em>segment</em>, which may contain a
  number of "multi" <em>blocks</em>, each representing the desired content
  translated in a given language, like
  <code>[language&#x2011;mark]&hellip;content&hellip;</code>, where:
  <ul>
    <li>
      <code>language&#x2011;mark</code> is the involved language code, such as
      <code>en</code>, <code>fr</code>, <code>es</code>&hellip;</li>
    <li>
      <code>&hellip;content&hellip;</code> is the translated content</li>
  </ul>
</p>
<p>
  Example:<br />
  <blockquote class="multilang-code"><code>
    &#91;&nbsp;multi&nbsp;&#93;<br />
    &#91;&nbsp;en&nbsp;&#93; This is an english text<br />
    &#91;&nbsp;fr&nbsp;&#93; Ceci est un texte en fran&ccedil;ais<br />
    &#91;&nbsp;/multi&nbsp;&#93;
  </code></blockquote>
</p>
<p>
  Any spaces, newlines, line-breaks or paragraph-breaks are ignored when they
  are located just after the opening tags, juste before the closing tags, and
  just around the language-marks. In other words, each text block is rendered
  trimmed.
</p>
</section><section>
<h3 class="multilang-toggler" id="user-interface">
  User interface
</h3>
<p>
  While <em>Multilang</em> fundamental work is to interpret the "multi" parts
  of text found to render it in the current language when displaying public
  pages, it also manages how these parts are presented to the user when it is
  creating or updating nodes or blocks contents.<br />
  Obviously at this moment the user must be able to look at all the available
  different translations of the same text: this is primarily achieved by
  merely displaying the raw "multi" syntax, as explained above, but it often
  may be a painful job to navigate through a lot of &#91;&nbsp;multi&nbsp;&#93;
  and &#91;&nbsp;lang&nbsp;&#93; tags.
</p>
<p>
  To allow a more friendly way to manage this, <em>Multilang</em> includes
  different UI facilities, depending on which input type is involved:
  <ul>
    <li>
      <strong>"long-text" fields</strong> (such as body), which represent 
      quantitatively the largest part of text and are input through
      <code>&lt; textarea &gt;</code> elements, may contain a lot of (even
      complex) HTML structures: it's why they're usually processed through a
      wysiwyg editor.<br />
      So for them <em>Multilang</em> includes a <em>CKEditor</em> plugin that
      can be activated for any text format (see
      <strong>Using with <em>CKEditor</em></strong> below).
    </li>
    <li>
      <strong>all other text fields</strong> (including "text", "link" and so
      on) are presented by Drupal through simple
      <code>&lt; input type="text" &gt;</code> elements.<br />
      As of 7.x-2.0, <em>Multilang</em> works as follows:
      <ol>
        <li>
          when the element content is already using "multi" syntax, with
          language blocks for each of the currently defined languages,
          attaches a widget (activated on focus) which displays each language
          part in a dedicated cell.
        </li>
        <li>
          when the element contains only raw text, or is already using "multi"
          syntax but with a limited set of languages, offers a "Use Multilang
          template" link, which on click:
          <ul>
            <li>
              automatically normalizes the element content, i.e. formats it 
              with the complete set of currently defined languages (if only raw
              text was present, it is affected to the default site language)
            </li>
            <li>attaches widget as explained above</li>
            <li>offers a "Back" link to restaure the original content</li>
          </ul>
        </li>
      </ol>
      This way the user remains free to normalize input or not (may be keeping
      only a limited set of defined languages), while he has an easy way to
      update when a new language is added to the site.
    </li>
    <li>
      by extension this behaviour is also available for texts which are not 
      really fields, such as <strong>field labels</strong> and <strong>node or 
      block title</strong>.
    </li>
  </ul>
</p>
</section><section>
<h3 class="multilang-toggler" id="ckeditor">
  Using with <em>CKEditor</em>
</h3>
<p>
  With the <em>CKEditor</em> module, you can benefit from improved input method
  which gets you rid of the above syntax and automatically offers input areas
  dedicated to each of the languages defined for the site.<br />
  For this to work:
  <ul>
    <li>
      your version of <em>CKEditor</em> must include the <em>Widget</em> plugin
      (you may install it from the <a href="http://ckeditor.com/builder">
      CKEditor builder</a>)</li>
    <li>
      in the <em>CKEditor</em> configuration, for each profile where you want
      to allow it, in the "EDITOR'S APPEARANCE" group:
      <ol>
        <li>
          in the "Tools bar" section, add the "Multilang" button to the tools
          bar</li>
        <li>
          in the "Plugins" section, check "Multilang" in the list of plugins
          to be activated</li>
      </ol>
    </li>
    <li>
      then with this profile any text field part where the "multi" syntax is
      used automatically displays a "MULTILANG" group, with a subgroup inside
      for each defined language</li>
    <li>
      at any time you may click the "Multilang" button in the tools bar to
      create a new empty "MULTILANG" group</li>
    <li>
      note that only text fields (or blocks text) can be entered through
      <em>CKEDITOR</em>: in views, and in nodes or blocks titles, you still
      must use the "multi" syntax, or the improved interface offered by the
      integrated widget (as of 7.x-2.0)</li>
</p>
</section><section>
<h3 class="multilang-toggler" id="pathauto">
  Using with <em>Pathauto</em>
</h3>
<p>
  With the <em>Pathauto</em> module, if you have introduced "multi" syntax in
  the node titles, you may use the
  <code>[node:multilang&#x2011;native&#x2011;title]</code> token to generate URL
  aliases, which will be localized using the <strong>site default
  language</strong>.
  <br />
  CAUTION: using the <code>[node:title]</code> token would generate aliases
  from the <strong>raw</strong> title, resulting in something like
  <code>multienmy&#x2011;titlefrmon&#x2011;titremulti</code>!
</p>
</section><section>
<h3 class="multilang-toggler" id="views">
  Using with <em>Views</em>
</h3>
<p>
  With the <em>Views</em> module, you can use the "multi" syntax inside of the
  texts you enter in the definition forms of a view.<br />
  They will be rendered like explained above, in the views summary, in the
  previews, and of course in the pages where they are included.
</p>
</section><section>
<h3 class="multilang-toggler" id="tips">
  Tips
</h3>
<ol>
  <li>
    Which language code is used to render depends on the lang part of the
    current URL (such as "en" in <code>http://example.com/en/&hellip;</code>),
    which is generally defined by how the lang switcher is currently set. If no 
    language is currently defined (so Drupal language is empty), the site lang 
    is used.<br />
    As a fortunate side effect, at any moment you may deactivate the lang
    switcher, and all contents including the "multi" syntax are simply rendered
    in the site lang.<br />
    CAUTION, though: at the time this document is written (Drupal 7.34),
    deactivate the lang switcher seems to drop already registered vocabulary
    terms translations!</li>
  <li>
    If a "multi" segment does not contain translation for the current lang, the
    available text in the site lang will be rendered instead.</li>
  <li>
    You may use the "multi" syntax not only in any text field or block body, but
    also in the node or block title, and in field labels.</li>
  <li>
    In order to allow a simple migration of contents from the Spip CMS, an
    alternative syntax is also accepted, using HTML-fashion tags such as
    <code>&lt;&nbsp;multi&nbsp;&gt;</code> and
    <code>&lt;&nbsp;/multi&nbsp;&gt;</code>, rather than
    <code>&#91;&nbsp;multi&nbsp;&#93;</code> and
    <code>&#91;&nbsp;/multi&nbsp;&#93;</code> (but language marks keep using
    square brackets, like in <code>&#91; en  &#93;</code>).<br />
    You may also use this syntax when manually entering text (using plain text
    editor).</li>
  <li>
    Becareful when directly importing data from an external source: due to the
    internal rules of the <em>HTML purifier</em> plugin, <em>CKEditor</em>
    breaks the <em>Multilang</em>'s automatic recognition of language blocks
    when they include <code>&lt; div &gt;</code> tags. You may consider to
    replace them by <code>&lt; section &gt;</code> tags.<br />
    Another solution could be to deactivate the <em>HTML purifier</em> plugin,
    so the &lt;div&gt; tags can be used, but this is not recommended, for
    general security reasons.</li>
</ol>
</section><section>
<h3 class="multilang-toggler" id="localization">
  Localization
</h3>
<p>
  Because of its mission to integrate multiple languages in the same container,
  the <em>Multilang</em> module does not conform to the standard Drupal
   localization system: in this domain, it is self-sufficient and embeds all
   of its own translations, written with the "multi" syntax.<br />
   All these translations (including the text you are reading now) are
   gathered in the file <code>multilang.data.inc</code>.
</p>
<p>
  To add a language, you just have to extend this file, in which the various
  "multi" segments required are divided into a few number of functions,
  standardized according to the following scheme:
  <blockquote class="multilang-code"><code>
    function _multilang_FUNCTION() {<br />
    &nbsp;&nbsp;module_load_include('inc', 'multilang', 'multilang.core');<br />
    &nbsp;&nbsp;ob_start();<br />
    ?&gt;<br />
    <strong>
    &#91;&nbsp;multi&nbsp;&#93;<br />
    &#91;&nbsp;en&nbsp;&#93; This is an english text<br />
    &#91;&nbsp;fr&nbsp;&#93; Ceci est un texte en fran&ccedil;ais<br />
    &#91;&nbsp;/multi&nbsp;&#93;<br />
    </strong>
    &lt;?php<br />
    &nbsp;&nbsp;return _multilang_process(ob_get_clean());<br />
    }
  </code></blockquote>
</p>
<p>
  In the example above, to add a Spanish translation will require you insert it
  in the "multi" segment, which will become:
  <blockquote class="multilang-code"><code>
    <strong>
    &#91;&nbsp;multi&nbsp;&#93;<br />
    &#91;&nbsp;en&nbsp;&#93; This is an english text<br />
    &#91;&nbsp;fr&nbsp;&#93; Ceci est un texte en fran&ccedil;ais<br />
    &#91;&nbsp;es&nbsp;&#93; Este es un texto en español<br />
    &#91;&nbsp;/multi&nbsp;&#93;<br />
    </strong>
  </code></blockquote>
</p>
<p>
  The <em>_multilang_token_info()</em> function is an exception to the above
  diagram, because the text it contains is distributed in a
  two-dimensional array, where each element is a "multi" segment. It is within 
  these that you will insert the new translations.
</p>
<p>
  <em>NOTE FOR ENGLISH SPEAKERS: the creator of this module is not 
  fluent in English, so it is unsure of the quality of writing. <b>All 
  corrections and improvements are welcome.</b></em>
</p>
</section>
[fr]
<p>
  Ce module est destiné à permettre la saisie de toutes les traductions 
  souhaitées, pour un texte donné, au sein d'un noeud unique, au lieu de 
  devoir créer pour chaque traduction un nouveau noeud (avec
  <em>Multilingual Content</em>) ou une version séparée de champ (avec
  <em>Entity Translation</em>).
</p>
<p>
  Cette méthode présente plusieurs avantages, notamment :
  <ul>
    <li>
      lorsqu'il saisit ou modifie un contenu, l'auteur conserve à tout moment 
      une vision globale sur toutes les traductions existantes</li>
    <li>
      consequence directe, on réduit le risque d'oublier de mettre à jour une 
      traduction lorsque le contenu original est modifié</li>
    <li>
      lorsqu'un contenu comporte une part de données qui ne changent pas avec 
      la langue (des images, des liens, ou simplement du HTML avec de nombreux 
      attributs), ces données n'ont pas besoin d'être dupliquées : seules les 
      parties textuelles doivent être saisies comme "multi" et ré-écrites dans
      les différentes langues</li>
    <li>
      parce qu'elle émule la syntaxe SPIP
      <code>&lt;&nbsp;multi&nbsp;&gt;</code>, cette méthode est particulièrement
      adaptée pour une migration directe des contenus issus de ce CMS, évitant
      ainsi un pénible travail de restructuration</li>
    <li>
      en outre, dans ce cas, les auteurs peuvent conserver leurs habitudes de 
      travail antérieures</li>
  </ul>
</p>
<section>
<h3 class="multilang-toggler" id="install">
  Installation/Configuration
</h3>
<p>
  <em>Multilang</em> fonctionne sans recourir à un autre module voué aux 
  traductions, dès l'instant où vous avez activé l'option
  "Internationalisation" de <em>i18n</em>, et configuré les langages souhaités.
  Cependant vous pouvez tout de même utiliser simultanément une ou plusieurs
  méthodes de traduction alternatives, comme <em>Multilingual Content</em>,
  <em>Menu translation</em> etc.
</p>
<p>
  Par lui-même, le module ne nécessite aucune configuration. Par contre, pour
  utiliser la syntaxe "multi" vous devrez :
  <ul>
    <li>
      dans "Administration > Configuration > Regionalisation et langue > 
      Langues", activer la méthode de détection "URL" et la configurer pour 
      utiliser "Préfixe de chemin"</li>
    <li>
      pour chaque langue activée, vous assurer d'avoir bien saisi le code
      standard correspondant dans la zone "Code de langue du préfixe de 
      chemin" (ne la laissez pas vide)</li>
    <li>
      placer le bloc <em>Sélecteur de langue (Texte de l'interface utilisateur)
      </em> dans une région de votre choix</li>
  </ul>
</p>
<p>
  Si vous utilisez également d'autres options du module <em>i18n</em>, vous
  devrez également prêter attention aux options de configuration suivantes :
  <ul>
    <li>
      pour chaque noeud dans lequel la syntaxe "multi" est utilisée, vous DEVEZ
      sélectionner <strong>Independant de la langue</strong> dans le champ
      "Langue" (sinon le sélecteur de langue désactiverait toutes les langues
      autres que celle spécifiée)</li>
    <li>
      pour chaque bloc dans lequel la syntaxe "multi" est utilisée, vous DEVEZ
      <strong>ne sélectionner aucune des langues proposées</strong> dans la
      partie "Langues" du groupe "Paramètres de visibilité"</li>
  </ul>
</p>
</section><section>
<h3 class="multilang-toggler" id="syntax">
  Syntaxe
</h3>
<p>
  NOTA: ceci est la structure interne qui permet à <em>Multilang</em> de
  fonctionner, initialement définie pour permettre une migration facile de
  données issues du CMS <em>SPIP</em>.<br />
  A partir de la version 7.x-2.0 vous pouvez totalement éviter de manipuler
  directement la syntaxe "multi" (voir <strong>Interface utilisateur</strong>
  ci-après). Cependant vous pouvez continuer à l'utiliser à votre convenance.
</p>
<p>
  Lorsque vous saisissez des données dans un champ de type texte, vous pouvez 
  insérer des portions de texte multilingues en les enfermant entre des 
  balises <code>&#91;&nbsp;multi&nbsp;&#93;</code> et
  <code>&#91;&nbsp;/multi&nbsp;&#93;</code>.<br />
  Une telle portion de texte est un <em>segment</em> "multi", qui peut
  contenir un certain nombre de <em>blocs</em> "multi", chacun représentant le
  contenu souhaité, traduit dans une langue donnée, sous la forme
  <code>[marqueur&#x2011;de&#x2011;langue]&hellip;contenu&hellip;</code>, où :
  <ul>
    <li>
      <code>marqueur&#x2011;de&#x2011;langue</code> est le code de langue
      concerné, par exemple <code>en</code>, <code>fr</code>,
      <code>es</code>&hellip;</li>
    <li>
      <code>&hellip;contenu&hellip;</code> est le contenu traduit</li>
  </ul>
</p>
<p>
  Exemple:<br />
  <blockquote class="multilang-code"><code>
    &#91;&nbsp;multi&nbsp;&#93;<br />
    &#91;&nbsp;en&nbsp;&#93; This is an english text<br />
    &#91;&nbsp;fr&nbsp;&#93; Ceci est un texte en fran&ccedil;ais<br />
    &#91;&nbsp;/multi&nbsp;&#93;
  </code></blockquote>
</p>
<p>
  Tous les espaces, caractères "nouvelle ligne", sauts de ligne ou sauts de 
  paragraphe HTML sont ignorés lorsqu'il se trouvent juste après les balises 
  ouvrantes, juste avant les balises fermantes, et précisément autour des 
  marques de code de langue. En d'autres termes, chaque bloc de texte est 
  rendu "nettoyé".
</p>
</section><section>
<h3 class="multilang-toggler" id="user-interface">
  Interface utilisateur
</h3>
<p>
  Outre son travail fondamental (interprêter les séquences de texte "multi"
  rencontrées, pour les rendre dans la langue actuelle lors de l'affichage des
  pages publiques), <em>Multilang</em> gère aussi la façon dont ces textes sont
  présentés à l'utilisateur lorsqu'il crée ou met à jour le contenu des noeuds
  ou des blocs.<br />
  Bien entendu, à ce moment l'utilisateur doit être en mesure de voir l'ensemble
  des différentes traductions du même texte : c'est ce qui est assuré de façon
  primaire par l'affichage du contenu brut de la syntaxe "multi", comme expliqué
  ci-avant, mais il peut souvent être pénible de devoir naviguer à travers les
  balises &#91;&nbsp;multi&nbsp;&#93; et &#91;&nbsp;lang&nbsp;&#93;.
</p>
<p>
  Pour rendre cette tâche plus confortable, l'interface utilisateur de
  <em>Multilang</em> comporte différentes facilités, selon le type d'entrée
  concerné :
  <ul>
    <li>
      <strong>les champs de type "long-text"</strong> (comme "body"), qui
      représentent quantitativement la plus grosse part de texte et sont saisis
      par l'intermédiaire d'éléments <cdoe>&lt; textarea &gt;</code>, peuvent
      contenir quantité de structures HTML, y compris complexes : c'est pourquoi
      ils sont généralement traités à l'aide d'un éditeur wysiwyg.<br />
      Aussi, pour ce qui les concerne, <em>Multilang</em> comporte un plugin
      <em>CKEditor</em> qui peut être activé pour n'importe quel format de texte
      (voir <strong>Utilisation avec <em>CKEditor</em></strong> ci-après).
    </li>
    <li>
      <strong>tous les autres champs texte</strong> (incluant "text", "link"
      etc) sont présentés par Drupal par l'intermédiaire de simples éléments
      <code>&lt; input type="text" &gt;</code>.<br />
      A partir de la version 7.x-2.0, <em>Multilang</em> fonctionne comme suit :
      <ol>
        <li>
          lorsque le contenu de l'élément utilise déjà la syntaxe "multi", avec
          un bloc de langue pour chacun des langages actuellement définis, il
          attache un widget (activé à la prise du focus) qui affiche chaque
          langage dans une cellule dédiée.
        </li>
        <li>
          lorsque l'élément ne contient que du texte brut, ou bien utilise déjà
          la syntaxe "multi" mais avec un ensemble limité de langages, il
          propose un lien "Utiliser le gabarit Multilang", qui au clic :
          <ul>
            <li>
              normalise automatiquement le contenu de l'élément, c'est-à-dire le
              formate avec un jeu complet des langages actuellement définis
              (si du texte brut seul était présent, il est affecté au langage
              par défaut du site)
            </li>
            <li>attache un widget comme expliqué ci-dessus</li>
            <li>propose un lien "Annuler" pour restaurer le contenu original
            </li>
          </ul>
        </li>
      </ol>
      Ainsi l'utilisateur reste libre de normaliser ou non la zone d'entrée
      (peut-être en ne conservant qu'un jeu limité de langages), tout en
      disposant d'un moyen facile de mise à jour lorsqu'un nouveau langage est
      ajouté au site.
    </li>
    <li>
      par extension, ce comportement est également disponible pour des textes 
      qui ne sont pas réellement des champs, comme les
      <strong>étiquettes de champ</strong> et les
      <strong>titres de noeuds et de blocs</strong>.
    </li>
  </ul>
</p>
</section><section>
<h3 class="multilang-toggler" id="ckeditor">
  Utilisation avec <em>CKEditor</em>
</h3>
<p>
  Avec le module <em>CKEditor</em>, vous pouvez bénéficier d'un mode de saisie
  amélioré qui fait abstraction de la syntaxe ci-dessus et vous propose
  automatiquement des zones de saisie dédiées à chacune des langues définies
  pour le site.<br />
  Pour utiliser cette possibilité :
  <ul>
    <li>
      votre version de <em>CKEditor</em> doit inclure le plugin <em>Widget</em>
      (vous pouvez l'installer à partir du
      <a href="http://ckeditor.com/builder">constructeur <em>CKEditor</em></a>)
      </li>
    <li>
      dans la configuration de <em>CKEditor</em>, pour chaque profil dans
      lequel vous voulez l'autoriser, dans le groupe "APPARENCE DE L'EDITEUR :
      <ol>
        <li>
          dans la section "Barre d'outils", ajoutez le bouton "Multilang" à
          la barre d'outils</li>
        <li>
          dans la section "Plugins", cochez "Multilang" dans la liste des
          plugins à activer</li>
      </ol>
    </li>
    <li>
      désormais, dans ce profil, toute partie de champ texte utilisant la
      syntaxe "multi" fait automatiquement apparaître un groupe "MULTILANG",
      à l'intérieur duquel un sous-groupe est affecté à chaque langue
      définie</li>
    <li>
      à tout moment, vous pouvez cliquer sur le bouton "Multilang" de la barre
      d'outils pour créer un nouveau groupe "MULTILANG" vide</li>
    <li>
      notez que seuls les champs texte (ou le texte des blocs) peuvent être
      saisis sous <em>CKEDITOR</em> : dans les vues, et dans les titres de
      noeuds ou de blocs, vous devrez toujours utiliser la syntaxe "multi",
      ou l'interface améliorée proposée par le widget intégré (à partir de la
      version 7.x-2.0)</li>
  </ul>
</p>
</section><section>
<h3 class="multilang-toggler" id="pathauto">
  Utilisation avec <em>Pathauto</em>
</h3>
<p>
  Avec le module <em>Pathauto</em>, si vous avez introduit la syntaxe "multi"
  dans le titre des noeuds, vous pouvez utiliser le jeton
  <code>[node:multilang&#x2011;native&#x2011;title]</code> pour générer des
  alias d'URL, qui seront localisés dans la
  <strong>langue par défaut du site</strong>.<br />
  ATTENTION : utiliser le jeton <code>[node:title]</code> générerait les alias
  à partir du <strong>texte brut</strong> du titre, pour donner quelque chose
  comme <code>multienmy&#x2011;titlefrmon&#x2011;titremulti</code> !
</p>
</section><section>
<h3 class="multilang-toggler" id="views">
  Utilisation avec <em>Views</em>
</h3>
<p>
  Avec le module <em>Views</em>, vous pouvez utiliser la syntaxe "multi" au
  sein des textes saisis dans les formulaires de définition d'une vue.<br />
  Ils seront rendus selon les règles exposées ci-dessus, dans le sommaire des
  vues, dans les aperçus, et bien sûr dans les pages où elles sont incluses.
</p>
</section><section>
<h3 class="multilang-toggler" id="tips">
  Conseils
</h3>
<ol>
  <li>
    Le code de langue choisi par le filtre pour extraire le texte dépend de la
    partie "langue" de l'URL actuelle (par exemple "fr" dans
    <code>http://exemple.com/fr/&hellip;</code>), qui est généralement définie
    par la situation actuelle du sélecteur de langue. Si aucune langue n'est 
    actuellement définie (la langue actuelle pour Drupal est donc vide), c'est
    la langue du site qui est utilisée.<br />
    Cet "effet de bord" intéressant vous permet à tout moment de désactiver le 
    sélecteur de langue, de telle sorte que tous les contenus utilisant le
    module Multilang seront simplement rendus dans la langue du site.<br />
    ATTENTION: au moment où ce document est rédigé (Drupal 7.34), il semble que
    la désactivation du sélecteur de langue provoque la perte des traductions
    des termes de taxonomie !</li>
  <li>
    Si un segment "multi" ne possède aucune traduction pour la langue actuelle,
    c'est le texte disponible dans la langue du site qui sera affiché.</li>
  <li>
    Vous pouvez utiliser la syntaxe "multi" dans n'importe quel champ texte ou
    corps de bloc, mais aussi dans le titre d'un noeud ou d'un bloc, ainsi que
    dans les étiquettes de champs.</li>
  <li>
    Pour une migration simplifiée de données en provenance du CMS SPIP, une 
    syntaxe alternative est également acceptée, utilisant des balises du type
    HTML comme <code>&lt;&nbsp;multi&nbsp;&gt;</code> et
    <code>&lt;&nbsp;/multi&nbsp;&gt;</code> à
    la place de <code>&#91;&nbsp;multi&nbsp;&#93;</code> et
    <code>&#91;&nbsp;/multi&nbsp;&#93;</code> (mais les blocs de langue
    emploient toujours les crochets, comme dans <code>
    &#91;&nbsp;fr&nbsp;&#93;</code>).<br />
    Vous pouvez aussi utiliser cette syntaxe alternative lorsque vous
    saisissez du texte manuellement (hors <em>CKEditor</em>).</li>
  <li>
    Attention lorsque vous importez directement des données depuis une source
    externe : les règles internes du plugin <em>HTML purifier</em> amènent
    <em>CKEditor</em> à interrompre la reconnaissance automatique des blocs de
    langue lorsqu'ils contiennent des balises <code>&lt; div &gt;</code>.
    Vous pouvez envisager de les remplacer par des balises
    <code>&lt; section &gt;</code> tags.<br />
    Un autre solution pourrait consister à désactiver le plugin
    <em>HTML purifier</em>, mais cela est déconseillé, pour des raisons
    générales de sécurité.</li>
</ol>
</section><section>
<h3 class="multilang-toggler" id="localization">
  Localisation
</h3>
<p>
  Du fait de sa vocation à intégrer plusieurs langues dans un même conteneur,
  le module <em>Multilang</em> ne se conforme pas à la norme Drupal en matière
  de localisation : sur ce plan, il est auto-suffisant et embarque la totalité
  de ses propres traductions, rédigées avec la syntaxe "multi".<br />
  Toutes ces traductions (y compris l'aide que vous lisez en ce moment) sont
  rassemblées dans le fichier <code>multilang.data.inc</code>.
</p>
<p>
  Pour ajouter une langue, il suffit donc d'enrichir ce fichier, dans lequel
  les différents segments "multi" nécessaires sont répartis dans un petit nombre
  de fonctions, standardisées selon le schéma suivant :
  <blockquote class="multilang-code"><code>
    function _multilang_FUNCTION() {<br />
    &nbsp;&nbsp;module_load_include('inc', 'multilang', 'multilang.core');<br />
    &nbsp;&nbsp;ob_start();<br />
    ?&gt;<br />
    <strong>
    &#91;&nbsp;multi&nbsp;&#93;<br />
    &#91;&nbsp;en&nbsp;&#93; This is an english text<br />
    &#91;&nbsp;fr&nbsp;&#93; Ceci est un texte en fran&ccedil;ais<br />
    &#91;&nbsp;/multi&nbsp;&#93;<br />
    </strong>
    &lt;?php<br />
    &nbsp;&nbsp;return _multilang_process(ob_get_clean());<br />
    }
  </code></blockquote>
</p>
<p>
  Dans l'exemple ci-dessus, pour ajouter une traduction en espagnol, il faudra
  l'insérer dans le segment "multi", qui deviendra :
  <blockquote class="multilang-code"><code>
    <strong>
    &#91;&nbsp;multi&nbsp;&#93;<br />
    &#91;&nbsp;en&nbsp;&#93; This is an english text<br />
    &#91;&nbsp;fr&nbsp;&#93; Ceci est un texte en fran&ccedil;ais<br />
    &#91;&nbsp;es&nbsp;&#93; Este es un texto en español<br />
    &#91;&nbsp;/multi&nbsp;&#93;<br />
    </strong>
  </code></blockquote>
</p>
<p>
  La fonction <em>_multilang_token_info()</em> constitue une exception au
  schema ci-dessus, car les textes qu'elle contient sont répartis dans un
  tableau à deux dimensions, où chaque élément est un segment "multi". C'est à 
  l'intérieur de ceux-ci qu'il faudra insérer les nouvelles traductions.
</p>
</section>
[/multi]
<section id="multilang-alert">
[multi]
[en]<strong>CAUTION</strong>: all tags mentioned in this page show spaces 
between square brackets and the tag content, such as <strong>[ multi ]</strong>, 
<strong>[ /multi ]</strong>, <strong>[ en ]</strong>, etc. Here these spaces
are only to prevent tags to be functional in the current context.<br />
When directly entering text in "multi" mode, <strong>never insert spaces 
inside of the tag body</strong>.
[fr]<strong>ATTENTION</strong> : toutes les balises citées dans cette page
font apparaître des espaces entre les crochets et le contenu de la balise,
comme dans <strong>[ multi ]</strong>, <strong>[ /multi ]</strong>,
<strong>[ en ]</strong>, etc. Ces espaces ne figurent ici que pour empêcher les
balises d'être fonctionnelles dans ce contexte.<br />
Lorsque vous saisissez directement du texte en mode "multi", <strong>n'insérez
jamais d'espaces dans le corps des balises</strong>.
[/multi]
</section>
<script>
(function($) {
$(document).ready(function() {
  $('head').append('\
<style>\
.multilang-toggler {\
  cursor: pointer;\
}\
.multilang-toggler::before {\
  content: "♦";\
}\
.multilang-toggler:hover {\
  background-color: #ddd;\
}\
code {\
  background-color: #e8e8e8;\
  margin: 0 .3em;\
}\
.multilang-code {\
  margin: 1em;\
  border-left: 5px solid #ccc;\
  padding-left: 1em;\
}\
.multilang-code * {\
  background-color: #fff;\
  margin: 0;\
}\
#multilang-alert {\
  position: fixed;\
  right: 0;\
  top: 70px;\
  margin: .5em 1em;\
  border-radius: .5em;\
  box-shadow: .5em .5em .3em #888;\
  background-color: #fcc;\
  padding: 1em;\
  width: 45em;\
}\
</style>\
  ');
  // set toggle capabilities to each chapter:
  $('.multilang-toggler').each(function() {
    $(this).siblings().hide();
    $(this).click(function() {
      $(this).siblings().toggle();
    });
  });
  // auto-expand a chapter if it is given as an anchor in the url:
  jQuery(document.location.hash).siblings()
    .css({backgroundColor:'#ff0'}).show(1000, 'linear', function() {
      jQuery(this).css({backgroundColor:'inherit'})
    });
});
})(jQuery);
</script>
<?php
  return _multilang_process(ob_get_clean());
}
/* ========================================================================== */
