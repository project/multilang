/**
 * @file
 * Multilang widget.
 *
 * This an attempt to friendly manage Multilang text parts when in edit process.
 *
 * Looks at the current form for any <input class="form-text"> elements, so
 * taking in account:
 * - all text fields (and their labels)
 * - other fields (and their labels) like link fields
 * - the node or block title
 * - any vocabulary name or taxonomy term
 *
 * For an element whose content is already using "multi" syntax, with language
 * blocks for each of the currently defined languages, attaches a widget
 * (activated on focus) which displays each language part in a dedicated cell.
 *
 * For each element containing only raw text, or already using "multi" syntax
 * but with a limited set of languages, offers a "Use Multilang template" link.
 * When clicked, this link:
 * - automatically normalizes the element content, i.e. formats it with the
 *   complete set of currently defined languages (if only raw text was present,
 *   it is affected to the default site language)
 * - attaches widget as explained above
 * - offers a "Back" link to restaure the original content
 *
 * CAUTION: at this time, the "real" text contained by the involved elements is
 * assumed not to include any "[" character (so currently regular expressions
 * used here remain simple).
 * The presence of "[" characters could lead to unexpected behaviours.
 *
 * NOTE: this regards only this widget, while the general Multilang mechanism
 * doesn't suffer this limitation.
 */
 
 /*
 Change rendering to render empty when language block present and empty
 (fallback happens only for absent or "lang-native" blocks)
 
 Widget for textarea? (node_type_form: description, title pattern; node_form: ?)
 */
(function($) {

var help = {},
    langs = {},
    defLang = '',
    curLang = '',
    filters = {},
    templateSkel = '',
    $inputArea = {},
    curInput = undefined,
    originalTexts = {},
    lookForSegment = new RegExp('\\[multi\\](.*?)\\[/multi\\]','ig');
    // CAUTION: currently, texts are assumed not to contain any "[".

$(document).ready(function() {

  // Prepare data.
  help = $.parseJSON(Drupal.settings.multilang.help);
  langs = Drupal.settings.multilang.langs;
  defLang = Drupal.settings.multilang.defLang;
  curLang = Drupal.settings.multilang.curLang;
  filters = Drupal.settings.multilang.filters;
  
  // Render current language in elements that require it.
  // *** should wait till admin-menu is built ***
  $(filters.toRender.join(',')).each(function() renderLang(this));
  
  // Create widget input area and template skeleton.
  $inputArea = $('<table id="multilang-area" />').appendTo('body');
  for (var lang in langs) {
    templateSkel += '[' + lang + ']';
    $inputArea.append(
      wrapTag(
        wrapTag(lang,'<th>') + wrapTag('<input />','<td>'),
        '<tr id="multilang-' + lang + '">'
      )
    );
  }
  templateSkel = wrapSegment(templateSkel);
  
  // Bind focus event everywhere else than multilang-area.
  $('body, body *').bind('focus.multilang', function(event) {
    // When focus outside of $inputArea, save changes and hide $inputArea.
    // (includes click to Submit/Preview/Cancel)
    if (curInput && !$(event.target).closest('#multilang-area').length) {
      // (or simply another cell is now focused in $inputArea)
      saveInput(event.target);
      stopInput();
    }
  });
  
  // Bind keyboard events on $inputArea.
  $('input',$inputArea).bind('keydown.multilang', function(event) {
    // When currently in $inputArea:
    // . if {Esc}, hide $inputArea.
    // ***. if {Ctrl-S}, save changes before submitting.*** currently dropped
    if (event.which == 27) {
      stopInput();
    }
    /*
    if (Drupal.form_save.controlDown && event.which == 83) {
      $(curInput).focus();
      $('input.form-save-default-button', Drupal.form_save.currentForm).click();
    }
    */
  });
  
  // Attach Multilang widget to any text field (and its label), plus title.
  $('input.form-text').not($(filters.noWidget.join(','))).each(prepareInput);
});

/**
 * Process element to render its content in the current language.
 */
function renderLang(element) {
  element.innerHTML = element.innerHTML.replace(
    lookForSegment,
    function(segment) {
      // Return current lang block, or a notice if absent, empty or "native".
      // CAUTION: currently, texts are assumed not to contain any "[".
      var result = lookForLang(segment, curLang);
      if (!result || result == wrap(langs[curLang], '...')) {
        result = putEmbeddedVars(help.noticeEmpty, {langNative: langs[curLang]});
        // TO REVISE: Customize result except when in a <head> child.
        if (!$(element).closest('head').length) {
          result = wrapTag(result,'<span class="multilang-notice">');
        }
      }
      return result;
    }
  );
}

/**
 * Prepare input, either attaching widget or offering to normalize if not yet.
 */
function prepareInput() {
  // Look for associated label.
  var $label = $(this).siblings('[for=' + this.id + ']');
  if ($label.length != 1) {
    // Not a really involved input.
    return;
  }
  if (this.value.replace(/\]([^\[]+)\[/ig,'\]\[') == templateSkel) {
    // Already normalized input, attach widget.
    attachWidget(this);
  } else {
    // Raw text or only partial set of languages, offer to normalize.
    $label.append(
      wrapTag(
        help.useText,
        '<a href="#" class="multilang-use" ' + 'title="' + help.useTitle + '">'
      )
      + wrapTag(
        help.backText,
        '<a href="#" class="multilang-back" ' + 'title="' + help.backTitle
        + '" style="display: none;">'
      )
    );
    $('.multilang-use',$label).bind('click.multilang', (function(element) {
      // Click "Use Multilang template" -> normalize <input>, attach widget.
      return function() {
        originalTexts[element.id] = element.value;
        element.value = normalizeText(element.value);
        $(element).siblings('label').find('a').toggle();
        stopInput();
        attachWidget(element);
      };
    })(this));
    $('.multilang-back',$label).bind('click.multilang', (function(element) {
      // Click "Back" -> detach widget and restaure original raw content.
      return function() {
        stopInput();
        $(element).unbind('focus.multilang').removeAttr('title');
        element.value = originalTexts[element.id];
        $(element).siblings('label').find('a').toggle();
      };
    })(this));
  }
}

/**
 * Attach widget to input element.
 */
function attachWidget(element) {
  $(element)
  .attr({title: help.inputTitle})
  .bind('focus.multilang', function() {
    curInput = this;
    // Distribute text among dedicated cells.
    for (var lang in langs) {
      $('#multilang-' + lang + ' input').val(
        lookForLang(this.value, lang)
      );
    }
    // Position Multilang input area to override original input.
    var xy = $(this).offset();
    $inputArea
    .css({
      top: xy.top + 'px',
      left: xy.left + 'px',
      width: $(this).outerWidth()
    })
    .show()
    .find('#multilang-' + curLang + ' input').focus()
    .end().find('input').css({
      fontFamily: $(this).css('fontFamily'),
      fontSize: $(this).css('fontSize')
    });
  });
}

/**
 * Normalize text to contain segments according to current languages set.
 */
function normalizeText(string) {
  // If @string is raw text, create a segment affected to the default language.
  if (!string.match(lookForSegment)) {
    string = wrapSegment('[' + defLang + ']' + string);
  }
  // Then reformat each segment found in @string to fit with template.
  return string.replace(
    lookForSegment,
    function(segment) {
      // Create a new segment populated from lang blocks found in original
      // segment. Result will contain all currently referenced langs, even
      // when absent in original segment. At the opposite, if unknown lang
      // blocks exist in original segment, they're lost.
      var newSegment = '';
      for (var lang in langs) {
        newSegment += langOrDefault(lookForLang(segment, lang), lang);
      }
      return wrapSegment(newSegment);
    }
  );
}

/**
 * Anciliary functions.
 */
function langOrDefault(string, lang) {
  // Return "[@lang]@string", where @string defaults to "...@lang-native..."
  return '[' + lang + ']' + (!!string ? string : wrap(langs[lang], '...'));
}
function lookForLang(string, lang) {
  // Look at @string to return a @lang block, or false if not found.
  // NOTE: @string is not necessary a segment, when it comes from raw input.
  var block = string.match(new RegExp('\\[' + lang + '\\]([^\\[]*)','i'));
  return !!block && block[1] ? block[1] : false;
}
function putEmbeddedVars(text, args) {
  // Equivalent of Drupal.formatString, for directly translated help.
  args = args || {};
  for (var argName in args) {
    text = text.replace(new RegExp('@' + argName), args[argName]);
  }
  return text;
}
function saveInput(element) {
  // Populates @element with a well-formed segment built from $inputArea.
  var newSegment = '';
  for (var lang in langs) {
    newSegment += langOrDefault($('#multilang-' + lang + ' input').val(), lang);
  }
  curInput.value = wrapSegment(newSegment);
}
function stopInput() {
  $inputArea.hide();
  curInput = undefined;
}
function wrapSegment(string) {
  return wrap(string, '[multi]', '[/multi]');
}
function wrapTag(string, tagOpen) {
  return wrap(string, tagOpen, tagOpen.replace(/^<([^ ]+).*/i,'</$1>'));
}
function wrap(text, before, after) {
  // Wrap @text between @before and @after, where @after defaults to @before.
  return before + text + (!!after ? after : before);
}

})(jQuery);
