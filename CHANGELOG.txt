Multilang 7.x-3.1, 2015-07-08
-----------------------------
 - Refoundation of customized tokens handling.

Multilang 7.x-3.0, 2015-06-20
-----------------------------
 - Now Multilang can be used also for taxonomy (vocabulary names, terms).

Multilang 7.x-2.2, 2015-06-14
-----------------------------
 - Fixed a regression bug regarding post-install message

Multilang 7.x-2.1, 2015-06-10
-----------------------------
 - Fixed #2491957: Multilang syntax abusively hidden when Views definition form 
   displayed 1st time

Multilang 7.x-2.0, 2015-06-07
-----------------------------
 - Fixed #2492501: Segment replaced by only current language if submit-error
   while adding new node
 - Dropped {Ctrl-S} shortcut (not really working)

Multilang 7.x-2.0-rc2, 2015-06-02
---------------------------------
 - Fixed #2498579: Importing data from external source may provide a fancy result
 - Added an alert message about typo to inline help

Multilang 7.x-2.0-rc1, 2015-05-31
--------------------------------
 - A widget was added to friendly manage "multi" syntax in all places where
   CKEditor doesn't work: text fields other than long-text, link fields, their
   labels, and block and node titles
 - Form title, breadcrumb and HTML title are now rendered in the current
   language, rather than displaying their raw "multi" content

Multilang 7.x-1.3, 2015-05-31
-----------------------------
 - Fixed #2498273: CKEditor's "Switch to plain text" link behaves like an url.

Multilang 7.x-1.2, 2015-05-30
-----------------------------
 - Fixed #2497705: "mailto:" links processed as internal links.

Multilang 7.x-1.1, 2015-05-19
-----------------------------
 - First public realease.
