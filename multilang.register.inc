<?php
/**
 * @file
 * Multilang register file.
 */

/*
Existing tokens for which to create "clone" Multilang tokens 'server side).
*/
function _multilang_tokens() {
  return array(
    'fields' => array(
      // not a type, used to register field types to create tokens for:
      'link_field',
      'taxonomy_term_reference',
      'text',
      'text_long',
      'text_with_summary',
    ),
    'node' => array(
      'body',
      'log',
      'summary',
      'title',
    ),
    'term' => array(
      'name',
      'description',
      'vocabulary',
    ),
    'vocabulary' => array(
      'name',
      'description',
    ),
  );
}
/*
Partial paths list to detect pages Multilang must NOT preprocess (server side).
*/
$multilang_exclude_urls = array(
  'node/(\d+/edit|add)',
  'block/(\d+/configure|add)',
  'taxonomy/(term/)?[^/]*/edit',
  'types/(manage|add)',
);
define('MULTILANG_DO_NOT_PROCESS',
  '#(' . implode('|', $multilang_exclude_urls) . ')#i');
/*
CSS selectors to process elements in not-preprocessed pages (client side).
*/
$multilang_toRender = array(
  'title',
  '#admin-menu-menu a',
  '.breadcrumb',
  '#branding .page-title',
  '.form-select',
);
define('MULTILANG_TO_RENDER', json_encode($multilang_toRender));
/*
CSS selectors to exclude elements from attaching widget (client side).
*** TBR: may be directly added into hook_init().
*** Or at the opposite keep adding from hook_form_alter() but doing it
*** individually from $form[_bade]_id recognition.
*/
$multilang_noWidget = array(
  // whole forms:
  '#contact-site-form *',
  '#ctools-export-ui-list-form *',
  '#pathauto-patterns-form *',
  '#user-login *',
  '#user-pass *',
  '#user-register-form *',
  // other elements:
  '#edit-weight', // most forms
  '#edit-date', // most forms
  #'#edit-search-block-form--2', ???
  '#edit-path-alias', // *_node_form:pathauto
  '.link-field-url input', // *_node_form:restricted_links
  '#edit-menu-link-title', // taxonomy term:pathauto
  '#edit-module-filter-name', // system_modules
  '#edit-page-path', // views_ui_add_form
  // ??? links: may be to keep ???
);
define('MULTILANG_NO_WIDGET', json_encode($multilang_noWidget));
/* ========================================================================== */
