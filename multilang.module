<?php
/**
 * @file
 * Multilang module file.
 *
 * Allows to embed multiple translations of the same text in a unique
 * node or user-defined block instance, using a Spip-like fashion.
 */

/* ============================================================== BASIC HOOKS */
/**
 * Implements hook_help().
 */
function multilang_help($path, $arg) {
  switch ($path) {
    case 'admin/help#multilang':
      // Return long help (see multilang.data.inc).
      module_load_include('inc', 'multilang', 'multilang.data');
      return _multilang_help();
  }
}
/**
 * Implements hook_init().
 *
 * This hook serves two intents:
 * - if currently responding to an Ajax request, starts buffering output, which
 *   will be resolved in hook_exit()
 * - if currently building a normal complete page, adds data to Drupal.settings
 *   for Multilang widget and Multilang CKEditor plugin
 */
function multilang_init() {
  if (strtolower(@$_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
    // This is an Ajax request.
    // Start buffering output and set a flag to notify hook_exit().
    drupal_static('multilang_ajax_ob', TRUE);
    ob_start();
    return;
  }
  // Otherwise this is a complete page.
  // Add Multilang widget.
  drupal_add_js('sites/all/modules/multilang/multilang.js');
  drupal_add_css('sites/all/modules/multilang/multilang.css');
  // Add Drupal settings for Multilang widget and Multilang CKEditor plugin.
  global $language;
  module_load_include('inc', 'multilang', 'multilang.data');
  // Save languages list (default first).
  $default = language_default();
  $langs[$default->language] = $default->native;
  foreach (language_list() as $lang) {
    if ($lang->language <> $default->language) {
      $langs[$lang->language] = $lang->native;
    }
  }
  module_load_include('inc', 'multilang', 'multilang.register');
  drupal_add_js(
    array(
      'multilang' => array(
        'help'    => _multilang_js_widget(),
        // (see multilang.data.inc).
        'langs'   => $langs,
        'defLang' => $default->language,
        'curLang' => $language->language,
        'filters' => array(
          // (see multilang.register.inc).
          'toRender' => json_decode(MULTILANG_TO_RENDER),
          'noWidget' => json_decode(MULTILANG_NO_WIDGET),
        ),
      ),
    ),
    'setting'
  );
}
/**
 * Implements hook_exit().
 *
 * If currently responding to an Ajax request, reports buffered output after
 * translating it if needed.
 */
function multilang_exit() {
  if (drupal_static('multilang_ajax_ob', FALSE)) {
    // Returning from Ajax request.
    // Echo buffered output after translating it if needed.
    if ($content = ob_get_clean()) {
      module_load_include('inc', 'multilang', 'multilang.core');
      if ($json = @json_decode($content)) {
        foreach ( $json as $i => $json_part) {
          // Look for "insert" command and process the corresponging content only.
          // So "viewsSetForm" content is not processed, and the user can see raw
          // text in "multi" segments.
          // NOTE: if json_part if not an object, do nothing.
          if (@$json_part->command == 'insert') {
            $json[$i]->data = _multilang_process(
              str_replace(
                array('\u003C', '\u003E', '\\/'),
                array('<', '>', '/'),
                $json[$i]->data
              ),
              /*links*/TRUE
            );
            $processed = TRUE;
          }
          // Back from JSON if changed.
          if (@$processed) {
            $content = json_encode($json);
          }
        }
      }
      echo $content;
    }
  }
}
/* ============================================================== FORMS HOOKS */
/**
 * Implements hook_form_alter().
 *
 * Add form-related information to Drupal.settings for Multilang widget.
 */
function multilang_form_alter(&$form, &$form_state, $form_id) {
  #fb($form_state,'FORM:'.$form_id);##
  #fb(field_info_field_types(),'field_types');##
  /*
  $form['#attached']['js'][] = array(
    'data' => array(
      'multilang' => array(
        'filters' => array(
          'noWidget' => json_decode(MULTILANG_NO_WIDGET),
        ),
      ),
    ),
    'type' => 'setting',
  );
  */
}
/* ================================================= CONTENT-PREPROCESS HOOKS */
/**
 * Implements hook_preprocess_block().
 *
 * Processes title and content of any (user-defined) block.
 */
function multilang_preprocess_block(&$variables) {
  module_load_include('inc', 'multilang', 'multilang.register');
  if (!preg_match(MULTILANG_DO_NOT_PROCESS, request_uri())) {
    // (don't work when currently in edit/add process).
    module_load_include('inc', 'multilang', 'multilang.core');
    $variables['block']->subject
      = _multilang_process($variables['block']->subject);
    $variables['content']
      = _multilang_process($variables['content']);
  }
}
/**
 * Implements hook_preprocess_html().
 *
 * Processes possibly remaining multi segments (node titles, breadcrumbs, Views
 * data...) and HTML <title>.
 */
function multilang_preprocess_html(&$variables) {
  /*
  global $trace; ##
  fb([$trace],'_TRACE');## (display tracing process into Firebug console)
  */
  module_load_include('inc', 'multilang', 'multilang.register');
  if (!preg_match(MULTILANG_DO_NOT_PROCESS, request_uri())) {
    // (don't work when currently in edit/add process).
    module_load_include('inc', 'multilang', 'multilang.core');
    $variables['page']['#children']
    // For node title and breadcrumbs.
      = _multilang_process($variables['page']['#children'], /*links*/TRUE);
    $variables['head_title']
    // For HTML <title>.
      = _multilang_process($variables['head_title']);
  }
}
/* ========================================================== FORMATTER HOOKS */
/**
 * Implements hook_field_formatter_info_alter().
 *
 * For text fields, graft multilang as prioritary formatter.
 */
function multilang_field_formatter_info_alter(&$info) {
  if ($info) {
    foreach ($info as $name => $formatter) {
      if (
        !@$formatter['settings']['multilang graft']
      and
        array_intersect($formatter['field types'],
          array('text', 'text_long', 'text_with_summary'))
      ) {
        // Substitute multilang to original module.
        $info[$name]['settings']['multilang graft'] = $formatter['module'];
        $info[$name]['module'] = 'multilang';
      }
    }
  }
}
/**
 * Implements hook_field_formatter_prepare_view().
 *
 * Extract current lang part, then renew args to the original formatter.
 */
function multilang_field_formatter_prepare_view(
  $entity_type, $entities, $field, $instances, $langcode, &$items, $displays
) {
  // Reduce items to current lang part.
  if ($items) {
    module_load_include('inc', 'multilang', 'multilang.core');
    foreach ($items as $nid => $node_data) {
      if ($node_data) {
        foreach ($node_data as $delta => $item) {
          if (isset($item['summary'])) {
            $items[$nid][$delta]['summary']
              = _multilang_process($item['summary'], /*links*/TRUE);
          }
          if (isset($item['value'])) {
            $items[$nid][$delta]['value']
              = _multilang_process($item['value'], /*links*/TRUE);
          }
        }
      }
    }
  }
  // Give original formatter a chance to execute its own hook.
  foreach ($displays as $display) {
    $hook = $display['settings']['multilang graft'] .
      '_field_formatter_prepare_view';
    if (function_exists($hook)) {
      $hook(
        $entity_type, $entities, $field, $instances, $langcode, $items, $displays
      );
    }
  }
}
/**
 * Implements hook_field_formatter_view().
 *
 * Simply renew args to the original formatter.
 */
function multilang_field_formatter_view(
  $entity_type, $entity, $field, $instance, $langcode, $items, $display
) {
  $hook = $display['settings']['multilang graft'] . '_field_formatter_view';
  if (function_exists($hook)) {
    return $hook(
      $entity_type, $entity, $field, $instance, $langcode, $items, $display
    );
  }
}
/**
 * Implements hook_field_formatter_settings_summary().
 *
 * Simply renew args to the original formatter.
 */
function multilang_field_formatter_settings_summary(
  $field, $instance, $view_mode
) {
  $hook
    = $instance['display'][$view_mode]['settings']['multilang graft'] .
    '_field_formatter_settings_summary';
  if (function_exists($hook)) {
    return $hook($field, $instance, $view_mode);
  }
}
/**
 * Implements hook_field_formatter_settings_form().
 *
 * Simply renew args to the original formatter.
 */
function multilang_field_formatter_settings_form(
  $field, $instance, $view_mode, $form, &$form_state
) {
  $hook
    = $instance['display'][$view_mode]['settings']['multilang graft'] .
    '_field_formatter_settings_form';
  if (function_exists($hook)) {
    return $hook($field, $instance, $view_mode, $form, $form_state);
  }
}
/* ============================================================== TOKEN HOOKS */
/**
 * Implements hook_token_info().
 *
 * Define [???:multilang(-native)-???] tokens.
 */
function multilang_token_info() {
  // Get full list of existing tokens (other than Multilang ones).
  global $_multilang_token_flag;
  if ($_multilang_token_flag) {
    // Avoid infinite loop when token_info() calls multilang_token_info() back.
    return array();
  }
  $_multilang_token_flag = TRUE;
  $token_info = token_info();
  // Prepare Multilang complements.
  module_load_include('inc', 'multilang', 'multilang.register');
  module_load_include('inc', 'multilang', 'multilang.core');
  $signature =
    ' ' . trim(_multilang_process(_multilang_token_adds()['signature']));
  $native_short =
    trim(_multilang_process(_multilang_token_adds()['native-short'])) .
    $signature;
  $native_long =
    trim(_multilang_process(_multilang_token_adds()['native-long'])) .
    $signature;
  // Add Multilang "clone" tokens for every registered template.
  $multilang_tokens = _multilang_tokens();
  foreach ($token_info['tokens'] as $type => $tokens) {
    if (array_key_exists($type, $multilang_tokens)) {
      foreach ($tokens as $token => $data) {
        if (substr($token, 0, 6) == 'field_') {
          $field_info = field_info_field($token);
          $select = in_array($field_info['type'],$multilang_tokens['fields']);
        } else {
          $select = in_array($token, $multilang_tokens[$type]);
        }
        if ($select) {
          $end_dot = substr($data['description'], -1) == '.' ? '.' : NULL;
          $description = $end_dot ?
            substr($data['description'], 0, -1) : $data['description'];
          $info['tokens'][$type]['multilang-' . $token] = array(
            'name'        => $data['name'] . $signature,
            'description' => $description . $signature . $end_dot,
          );
          $info['tokens'][$type]['multilang-native-' . $token] = array(
            'name'        => $data['name'] . ' ' . $native_short,
            'description' => $description . ' ' . $native_long . $end_dot,
          );
        }
      }
    }
  }
  return $info;
}
/**
 * Implements hook_tokens().
 *
 * For any [<type>:multilang(-native)-<token>], apply _multilang_process() to
 * the corresponding [<type>:<token>].
 */
function multilang_tokens(
  $type, $tokens, array $data = array(), array $options = array()
) {
  // @todo Remove ## when https://www.drupal.org/node/2525802 is fixed.
  static $done = array(); ##
  if (array_key_exists($type, _multilang_tokens())) {
    module_load_include('inc', 'multilang', 'multilang.core');
    foreach ($tokens as $name => $raw_token) {
      if (preg_match('`^multilang-(native-)?(.*)$`i', $name, $matches)) {
        if (in_array($name, $done)) { ##
          // This token has already been furnished. ##
          continue; ##
        } ##
        $done[] = $name; ##
        $replacements[$raw_token] = _multilang_process(
          token_replace('[' . $type . ':' . $matches[2] . ']', $data),
          /*links*/FALSE, /*default*/$matches[1]
        );
      }
    }
    if (@$replacements) {
      return $replacements;
    }
  }
}
/* =========================================================== CKEDITOR HOOKS */
/**
 * Implements hook_ckeditor_plugin().
 *
 * Add Multilang ckeditor plugin.
 */
function multilang_ckeditor_plugin() {
  module_load_include('inc', 'multilang', 'multilang.data');
  return array(
    'multilang' => array(
      'name' => 'multilang',
      'desc' => _multilang_ckeditor_plugin(),
      // (see multilang.data.inc).
      'path' => drupal_get_path('module', 'multilang') . '/ckeditor/',
      'buttons' => array(
        'multilang' => array(
          'label' => 'Multilang',
          'icon' => 'multilang.png',
        ),
      ),
    ),
  );
}
/* ========================================================================== */
/*
function _trace($init = NULL) {
  static $base;
  $all = debug_backtrace();
  // Trace process starts only at the 1st "_trace(TRUE);" invocation.
  if ($init AND !$base) {
    $base = count($all);
  }
  if ($base) {
    $level = count($all) - $base;
    global $trace;
    $trace .=
      "\n" . str_repeat('--', $level) .
      $all[1]['function'] . '(' . $all[1]['args'][0] . ')' .
      (@$all[1]['file'] ?
        (', ' . basename($all[1]['file']) . ': ' . $all[1]['line']) : null);
  }
}
*/
