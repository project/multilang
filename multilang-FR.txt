<?php
/**
 * @file
 * Multilang module text data.
 *
 * Contains all texts translations, written using "multi" syntax.
 */

/* ================================================================== INSTALL */
/**
 * Informative message after module activation.
 */
function _multilang_install() {
  module_load_include('inc', 'multilang', 'multilang.core');
  ob_start();
?>
[multi]
[fr]Le module <em>Multilang</em> a été installé avec succès.<br />
Vous pouvez désormais utiliser la syntaxe "multi" à l\'intérieur des champs
texte, dans n\'importe quel type de contenu ainsi que dans les blocs.<br />
NOTA : pour utiliser les
<a href="<?php echo _MULBASE_PATH; ?>/fr/admin/help/multilang#ckeditor">
possibilités avancées</a> disponibles avec CKEditor, vous devez installer le
plugin <em>Widget</em>.
[/multi]
<?php
  return _multilang_process(ob_get_clean());
}
/* ================================================================= FALLBACK */
/**
 * Tip displayed when a "multi" block doesn't contain the required language.
 */
function _multilang_fallback() {
  module_load_include('inc', 'multilang', 'multilang.core');
  ob_start();
?>
[multi]
[fr]Cette section n'a pas encore été traduite. Merci de votre compréhension.
[/multi]
<?php
  // Special case: default=NULL notifies not to fallback when required
  // language is not found (or would enter infinite loop).
  return _multilang_process(ob_get_clean(), /*links*/FALSE, /*default*/NULL);
}
/* =================================================================== TOKENS */
/**
 * Localized tokens characteristics.
 */
function _multilang_token_info() {
  return array(
    'multilang-title' => array(
      'name' => '
[multi]
[fr]Titre avec Multilang
[/multi]
      ',
      'description' => '
[multi]
[fr]Le titre du noeud, localisé avec Multilang
[/multi]
      ',
    ),
    'multilang-native-title' => array(
      'name' => '
[multi]
[fr]Titre (natif) avec Multilang
[/multi]
      ',
      'description' => '
[multi]
[fr]Le titre du noeud, dans la langue du site par défaut avec Multilang
[/multi]
      ',
    ),
  );
}
/* ================================================================ JS WIDGET */
/**
 * Informative text used in multilang.js widget.
 */
function _multilang_js_widget() {
  module_load_include('inc', 'multilang', 'multilang.core');
  ob_start();
  // CAUTION: this is JSON data.
?>
{
  "backText":
[multi]
[fr]"Annuler"
[/multi],
  "backTitle":
[multi]
[fr]"Restaurer le contenu précédent"
[/multi],
  "noticeEmpty":
[multi]
[fr]"texte @langNative vide"
[/multi],
  "useText":
[multi]
[fr]"Utiliser le gabarit Multilang"
[/multi],
  "useTitle":
[multi]
[fr]"Reformater cette zone en fonction des langues actuellement définies"
[/multi]
}
<?php
  return _multilang_process(ob_get_clean());
}
/* ================================================================= CKEDITOR */
/**
 * Informative text displayed in CKEditor config plugins list.
 */
function _multilang_ckeditor_plugin() {
  module_load_include('inc', 'multilang', 'multilang.core');
  ob_start();
?>
[multi]
[fr]Multilang - Un plugin pour intégrer des traductions en plusieurs langues 
dans le même champ
[/multi]
<?php
  return _multilang_process(ob_get_clean());
}
/* ===================================================================== HELP */
/**
 * Complete documentation.
 */
function _multilang_help() {
  module_load_include('inc', 'multilang', 'multilang.core');
  ob_start();
?>
<h3>
  <em>Multilang</em>
</h3>
[multi]
[fr]
<p>
  Ce module est destiné à permettre la saisie de toutes les traductions 
  souhaitées, pour un texte donné, au sein d'un noeud unique, au lieu de 
  devoir créer pour chaque traduction un nouveau noeud (avec
  <em>Multilingual Content</em>) ou une version séparée de champ (avec
  <em>Entity Translation</em>).
</p>
<p>
  Cette méthode présente plusieurs avantages, notamment :
  <ul>
    <li>
      lorsqu'il saisit ou modifie un contenu, l'auteur conserve à tout moment 
      une vision globale sur toutes les traductions existantes</li>
    <li>
      consequence directe, on réduit le risque d'oublier de mettre à jour une 
      traduction lorsque le contenu original est modifié</li>
    <li>
      lorsqu'un contenu comporte une part de données qui ne changent pas avec 
      la langue (des images, des liens, ou simplement du HTML avec de nombreux 
      attributs), ces données n'ont pas besoin d'être dupliquées : seules les 
      parties textuelles doivent être saisies comme "multi" et ré-écrites dans
      les différentes langues</li>
    <li>
      parce qu'elle émule la syntaxe SPIP
      <code>&lt;&nbsp;multi&nbsp;&gt;</code>, cette méthode est particulièrement
      adaptée pour une migration directe des contenus issus de ce CMS, évitant
      ainsi un pénible travail de restructuration</li>
    <li>
      en outre, dans ce cas, les auteurs peuvent conserver leurs habitudes de 
      travail antérieures</li>
  </ul>
</p>
<section>
<h3 class="multilang-toggler" id="install">
  Installation/Configuration
</h3>
<p>
  <em>Multilang</em> fonctionne sans recourir à un autre module voué aux 
  traductions, dès l'instant où vous avez activé l'option
  "Internationalisation" de <em>i18n</em>, et configuré les langages souhaités.
  Cependant vous pouvez tout de même utiliser simultanément une ou plusieurs
  méthodes de traduction alternatives, comme <em>Multilingual Content</em>,
  <em>Menu translation</em> etc.
</p>
<p>
  Par lui-même, le module ne nécessite aucune configuration. Par contre, pour
  utiliser la syntaxe "multi" vous devrez :
  <ul>
    <li>
      dans "Administration > Configuration > Regionalisation et langue > 
      Langues", activer la méthode de détection "URL" et la configurer pour 
      utiliser "Préfixe de chemin"</li>
    <li>
      pour chaque langue activée, vous assurer d'avoir bien saisi le code
      standard correspondant dans la zone "Code de langue du préfixe de 
      chemin" (ne la laissez pas vide)</li>
    <li>
      placer le bloc <em>Sélecteur de langue (Texte de l'interface utilisateur)
      </em> dans une région de votre choix</li>
  </ul>
</p>
<p>
  Si vous utilisez également d'autres options du module <em>i18n</em>, vous
  devrez également prêter attention aux options de configuration suivantes :
  <ul>
    <li>
      pour chaque noeud dans lequel la syntaxe "multi" est utilisée, vous DEVEZ
      sélectionner <strong>Independant de la langue</strong> dans le champ
      "Langue" (sinon le sélecteur de langue désactiverait toutes les langues
      autres que celle spécifiée)</li>
    <li>
      pour chaque bloc dans lequel la syntaxe "multi" est utilisée, vous DEVEZ
      <strong>ne sélectionner aucune des langues proposées</strong> dans la
      partie "Langues" du groupe "Paramètres de visibilité"</li>
  </ul>
</p>
</section><section>
<h3 class="multilang-toggler" id="syntax">
  Syntaxe
</h3>
<p>
  NOTA: ceci est la structure interne qui permet à <em>Multilang</em> de
  fonctionner, initialement définie pour permettre une migration facile de
  données issues du CMS <em>SPIP</em>.<br />
  A partir de la version 7.x-2.0 vous pouvez totalement éviter de manipuler
  directement la syntaxe "multi" (voir <strong>Interface utilisateur</strong>
  ci-après). Cependant vous pouvez continuer à l'utiliser à votre convenance.
</p>
<p>
  Lorsque vous saisissez des données dans un champ de type texte, vous pouvez 
  insérer des portions de texte multilingues en les enfermant entre des 
  balises <code>&#91;&nbsp;multi&nbsp;&#93;</code> et
  <code>&#91;&nbsp;/multi&nbsp;&#93;</code>.<br />
  Une telle portion de texte est un <em>segment</em> "multi", qui peut
  contenir un certain nombre de <em>blocs</em> "multi", chacun représentant le
  contenu souhaité, traduit dans une langue donnée, sous la forme
  <code>[marqueur&#x2011;de&#x2011;langue]&hellip;contenu&hellip;</code>, où :
  <ul>
    <li>
      <code>marqueur&#x2011;de&#x2011;langue</code> est le code de langue
      concerné, par exemple <code>en</code>, <code>fr</code>,
      <code>es</code>&hellip;</li>
    <li>
      <code>&hellip;contenu&hellip;</code> est le contenu traduit</li>
  </ul>
</p>
<p>
  Exemple:<br />
  <blockquote class="multilang-code"><code>
    &#91;&nbsp;multi&nbsp;&#93;<br />
    &#91;&nbsp;en&nbsp;&#93; This is an english text<br />
    &#91;&nbsp;fr&nbsp;&#93; Ceci est un texte en fran&ccedil;ais<br />
    &#91;&nbsp;/multi&nbsp;&#93;
  </code></blockquote>
</p>
<p>
  Tous les espaces, caractères "nouvelle ligne", sauts de ligne ou sauts de 
  paragraphe HTML sont ignorés lorsqu'il se trouvent juste après les balises 
  ouvrantes, juste avant les balises fermantes, et précisément autour des 
  marques de code de langue. En d'autres termes, chaque bloc de texte est 
  rendu "nettoyé".
</p>
</section><section>
<h3 class="multilang-toggler" id="user-interface">
  Interface utilisateur
</h3>
<p>
  Outre son travail fondamental (interprêter les séquences de texte "multi"
  rencontrées, pour les rendre dans la langue actuelle lors de l'affichage des
  pages publiques), <em>Multilang</em> gère aussi la façon dont ces textes sont
  présentés à l'utilisateur lorsqu'il crée ou met à jour le contenu des noeuds
  ou des blocs.<br />
  Bien entendu, à ce moment l'utilisateur doit être en mesure de voir l'ensemble
  des différentes traductions du même texte : c'est ce qui est assuré de façon
  primaire par l'affichage du contenu brut de la syntaxe "multi", comme expliqué
  ci-avant, mais il peut souvent être pénible de devoir naviguer à travers les
  balises &#91;&nbsp;multi&nbsp;&#93; et &#91;&nbsp;lang&nbsp;&#93;.
</p>
<p>
  Pour rendre cette tâche plus confortable, l'interface utilisateur de
  <em>Multilang</em> comporte différentes facilités, selon le type d'entrée
  concerné :
  <ul>
    <li>
      <strong>les champs de type "long-text"</strong> (comme "body"), qui
      représentent quantitativement la plus grosse part de texte et sont saisis
      par l'intermédiaire d'éléments <cdoe>&lt; textarea &gt;</code>, peuvent
      contenir quantité de structures HTML, y compris complexes : c'est pourquoi
      ils sont généralement traités à l'aide d'un éditeur wysiwyg.<br />
      Aussi, pour ce qui les concerne, <em>Multilang</em> comporte un plugin
      <em>CKEditor</em> qui peut être activé pour n'importe quel format de texte
      (voir <strong>Utilisation avec <em>CKEditor</em></strong> ci-après).
    </li>
    <li>
      <strong>tous les autres champs texte</strong> (incluant "text", "link"
      etc) sont présentés par Drupal par l'intermédiaire de simples éléments
      <code>&lt; input type="text" &gt;</code>.<br />
      A partir de la version 7.x-2.0, <em>Multilang</em> fonctionne comme suit :
      <ol>
        <li>
          lorsque le contenu de l'élément utilise déjà la syntaxe "multi", avec
          un bloc de langue pour chacun des langages actuellement définis, il
          attache un widget (activé à la prise du focus) qui affiche chaque
          langage dans une cellule dédiée.
        </li>
        <li>
          lorsque l'élément ne contient que du texte brut, ou bien utilise déjà
          la syntaxe "multi" mais avec un ensemble limité de langages, il
          propose un lien "Utiliser le gabarit Multilang", qui au clic :
          <ul>
            <li>
              normalise automatiquement le contenu de l'élément, c'est-à-dire le
              formate avec un jeu complet des langages actuellement définis
              (si du texte brut seul était présent, il est affecté au langage
              par défaut du site)
            </li>
            <li>attache un widget comme expliqué ci-dessus</li>
            <li>propose un lien "Annuler" pour restaurer le contenu original
            </li>
          </ul>
        </li>
      </ol>
      Ainsi l'utilisateur reste libre de normaliser ou non la zone d'entrée
      (peut-être en ne conservant qu'un jeu limité de langages), tout en
      disposant d'un moyen facile de mise à jour lorsqu'un nouveau langage est
      ajouté au site.
    </li>
    <li>
      par extension, ce comportement est également disponible pour des textes 
      qui ne sont pas réellement des champs, comme les
      <strong>étiquettes de champ</strong> et les
      <strong>titres de noeuds et de blocs</strong>.
    </li>
  </ul>
</p>
</section><section>
<h3 class="multilang-toggler" id="ckeditor">
  Utilisation avec <em>CKEditor</em>
</h3>
<p>
  Avec le module <em>CKEditor</em>, vous pouvez bénéficier d'un mode de saisie
  amélioré qui fait abstraction de la syntaxe ci-dessus et vous propose
  automatiquement des zones de saisie dédiées à chacune des langues définies
  pour le site.<br />
  Pour utiliser cette possibilité :
  <ul>
    <li>
      votre version de <em>CKEditor</em> doit inclure le plugin <em>Widget</em>
      (vous pouvez l'installer à partir du
      <a href="http://ckeditor.com/builder">constructeur <em>CKEditor</em></a>)
      </li>
    <li>
      dans la configuration de <em>CKEditor</em>, pour chaque profil dans
      lequel vous voulez l'autoriser, dans le groupe "APPARENCE DE L'EDITEUR :
      <ol>
        <li>
          dans la section "Barre d'outils", ajoutez le bouton "Multilang" à
          la barre d'outils</li>
        <li>
          dans la section "Plugins", cochez "Multilang" dans la liste des
          plugins à activer</li>
      </ol>
    </li>
    <li>
      désormais, dans ce profil, toute partie de champ texte utilisant la
      syntaxe "multi" fait automatiquement apparaître un groupe "MULTILANG",
      à l'intérieur duquel un sous-groupe est affecté à chaque langue
      définie</li>
    <li>
      à tout moment, vous pouvez cliquer sur le bouton "Multilang" de la barre
      d'outils pour créer un nouveau groupe "MULTILANG" vide</li>
    <li>
      notez que seuls les champs texte (ou le texte des blocs) peuvent être
      saisis sous <em>CKEDITOR</em> : dans les vues, et dans les titres de
      noeuds ou de blocs, vous devrez toujours utiliser la syntaxe "multi",
      ou l'interface améliorée proposée par le widget intégré (à partir de la
      version 7.x-2.0)</li>
  </ul>
</p>
</section><section>
<h3 class="multilang-toggler" id="pathauto">
  Utilisation avec <em>Pathauto</em>
</h3>
<p>
  Avec le module <em>Pathauto</em>, si vous avez introduit la syntaxe "multi"
  dans le titre des noeuds, vous pouvez utiliser le jeton
  <code>[node:multilang&#x2011;native&#x2011;title]</code> pour générer des
  alias d'URL, qui seront localisés dans la
  <strong>langue par défaut du site</strong>.<br />
  ATTENTION : utiliser le jeton <code>[node:title]</code> générerait les alias
  à partir du <strong>texte brut</strong> du titre, pour donner quelque chose
  comme <code>multienmy&#x2011;titlefrmon&#x2011;titremulti</code> !
</p>
</section><section>
<h3 class="multilang-toggler" id="views">
  Utilisation avec <em>Views</em>
</h3>
<p>
  Avec le module <em>Views</em>, vous pouvez utiliser la syntaxe "multi" au
  sein des textes saisis dans les formulaires de définition d'une vue.<br />
  Ils seront rendus selon les règles exposées ci-dessus, dans le sommaire des
  vues, dans les aperçus, et bien sûr dans les pages où elles sont incluses.
</p>
</section><section>
<h3 class="multilang-toggler" id="tips">
  Conseils
</h3>
<ol>
  <li>
    Le code de langue choisi par le filtre pour extraire le texte dépend de la
    partie "langue" de l'URL actuelle (par exemple "fr" dans
    <code>http://exemple.com/fr/&hellip;</code>), qui est généralement définie
    par la situation actuelle du sélecteur de langue. Si aucune langue n'est 
    actuellement définie (la langue actuelle pour Drupal est donc vide), c'est
    la langue du site qui est utilisée.<br />
    Cet "effet de bord" intéressant vous permet à tout moment de désactiver le 
    sélecteur de langue, de telle sorte que tous les contenus utilisant le
    module Multilang seront simplement rendus dans la langue du site.<br />
    ATTENTION: au moment où ce document est rédigé (Drupal 7.34), il semble que
    la désactivation du sélecteur de langue provoque la perte des traductions
    des termes de taxonomie !</li>
  <li>
    Si un segment "multi" ne possède aucune traduction pour la langue actuelle,
    c'est le texte disponible dans la langue du site qui sera affiché.</li>
  <li>
    Vous pouvez utiliser la syntaxe "multi" dans n'importe quel champ texte ou
    corps de bloc, mais aussi dans le titre d'un noeud ou d'un bloc, ainsi que
    dans les étiquettes de champs.</li>
  <li>
    Pour une migration simplifiée de données en provenance du CMS SPIP, une 
    syntaxe alternative est également acceptée, utilisant des balises du type
    HTML comme <code>&lt;&nbsp;multi&nbsp;&gt;</code> et
    <code>&lt;&nbsp;/multi&nbsp;&gt;</code> à
    la place de <code>&#91;&nbsp;multi&nbsp;&#93;</code> et
    <code>&#91;&nbsp;/multi&nbsp;&#93;</code> (mais les blocs de langue
    emploient toujours les crochets, comme dans <code>
    &#91;&nbsp;fr&nbsp;&#93;</code>).<br />
    Vous pouvez aussi utiliser cette syntaxe alternative lorsque vous
    saisissez du texte manuellement (hors <em>CKEditor</em>).</li>
  <li>
    Attention lorsque vous importez directement des données depuis une source
    externe : les règles internes du plugin <em>HTML purifier</em> amènent
    <em>CKEditor</em> à interrompre la reconnaissance automatique des blocs de
    langue lorsqu'ils contiennent des balises <code>&lt; div &gt;</code>.
    Vous pouvez envisager de les remplacer par des balises
    <code>&lt; section &gt;</code> tags.<br />
    Un autre solution pourrait consister à désactiver le plugin
    <em>HTML purifier</em>, mais cela est déconseillé, pour des raisons
    générales de sécurité.</li>
</ol>
</section><section>
<h3 class="multilang-toggler" id="localization">
  Localisation
</h3>
<p>
  Du fait de sa vocation à intégrer plusieurs langues dans un même conteneur,
  le module <em>Multilang</em> ne se conforme pas à la norme Drupal en matière
  de localisation : sur ce plan, il est auto-suffisant et embarque la totalité
  de ses propres traductions, rédigées avec la syntaxe "multi".<br />
  Toutes ces traductions (y compris l'aide que vous lisez en ce moment) sont
  rassemblées dans le fichier <code>multilang.data.inc</code>.
</p>
<p>
  Pour ajouter une langue, il suffit donc d'enrichir ce fichier, dans lequel
  les différents segments "multi" nécessaires sont répartis dans un petit nombre
  de fonctions, standardisées selon le schéma suivant :
  <blockquote class="multilang-code"><code>
    function _multilang_FUNCTION() {<br />
    &nbsp;&nbsp;module_load_include('inc', 'multilang', 'multilang.core');<br />
    &nbsp;&nbsp;ob_start();<br />
    ?&gt;<br />
    <strong>
    &#91;&nbsp;multi&nbsp;&#93;<br />
    &#91;&nbsp;en&nbsp;&#93; This is an english text<br />
    &#91;&nbsp;fr&nbsp;&#93; Ceci est un texte en fran&ccedil;ais<br />
    &#91;&nbsp;/multi&nbsp;&#93;<br />
    </strong>
    &lt;?php<br />
    &nbsp;&nbsp;return _multilang_process(ob_get_clean());<br />
    }
  </code></blockquote>
</p>
<p>
  Dans l'exemple ci-dessus, pour ajouter une traduction en espagnol, il faudra
  l'insérer dans le segment "multi", qui deviendra :
  <blockquote class="multilang-code"><code>
    <strong>
    &#91;&nbsp;multi&nbsp;&#93;<br />
    &#91;&nbsp;en&nbsp;&#93; This is an english text<br />
    &#91;&nbsp;fr&nbsp;&#93; Ceci est un texte en fran&ccedil;ais<br />
    &#91;&nbsp;es&nbsp;&#93; Este es un texto en español<br />
    &#91;&nbsp;/multi&nbsp;&#93;<br />
    </strong>
  </code></blockquote>
</p>
<p>
  La fonction <em>_multilang_token_info()</em> constitue une exception au
  schema ci-dessus, car les textes qu'elle contient sont répartis dans un
  tableau à deux dimensions, où chaque élément est un segment "multi". C'est à 
  l'intérieur de ceux-ci qu'il faudra insérer les nouvelles traductions.
</p>
</section>
[/multi]
<section id="multilang-alert">
[multi]
[fr]<strong>ATTENTION</strong> : toutes les balises citées dans cette page
font apparaître des espaces entre les crochets et le contenu de la balise,
comme dans <strong>[ multi ]</strong>, <strong>[ /multi ]</strong>,
<strong>[ en ]</strong>, etc. Ces espaces ne figurent ici que pour empêcher les
balises d'être fonctionnelles dans ce contexte.<br />
Lorsque vous saisissez directement du texte en mode "multi", <strong>n'insérez
jamais d'espaces dans le corps des balises</strong>.
[/multi]
</section>
<?php
  return _multilang_process(ob_get_clean());
}
/* ========================================================================== */
